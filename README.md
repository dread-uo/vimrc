# `dread-uo` nvim config

This is an attempt to not reconfigure my editor settings and plugins everytime
I put my hands on a new machine or have my machines formatted.

If you're looking for a better documented `vimrc`, you are looking for
[marcheing's](https://gitlab.com/marcheing/vimrc/) dotfiles. If you are not
looking for that, I still suggest that you read it, anyway.

I'll probably only use `neovim`, so I didn't care about compatibility.

If you have some suggestions, don't hesitate to get in touch.
