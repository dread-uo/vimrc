#!/usr/bin/env bash

set -e

# Installs vim-plug
echo "Installing vim plug..."
curl -s -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Check/Create neovim config directory
mkdir -p ~/.config/nvim/

# Link relevant files
echo "Linking configuration files..."
ln -f init.vim ~/.config/nvim/init.vim
ln -f .editorconfig ~/.editorconfig

nvim -u <( sed '/^colo/d' init.vim ) -c "PlugInstall | qa"

echo "Done!"
