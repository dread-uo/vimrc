" True colors o/
set termguicolors

" TODO:
" Take a look at 'magic' config for regex
" set magic

" Maybe set a margin?
" set foldcolumn=1

" Useful for checking options and completing some commands
set wildmenu

" Useful for yanking/deleting/commenting a range of lines
set number
set relativenumber

" It's funny when I show some code to people, and they get my mouse to select
" some variable, and it doesn't work as they expect, but I guess I'll make it
" easier for them
set mouse=a

" Plugins section
call plug#begin('~/.local/share/nvim/plugged')

" Fancy filesystem tree
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" All the language definitions I'll ever use are here :)
Plug 'sheerun/vim-polyglot'
Plug 'leafgarland/typescript-vim'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'rust-lang/rust.vim'

" Sometimes, it's nice to have some aligned bars or other stuff
Plug 'junegunn/vim-easy-align'

" Allows me to easily (un)comment lines and blocks of code
Plug 'tpope/vim-commentary'

" Colorizes rgb and hex colors on source files
Plug 'lilydjwg/colorizer'

" Closes brackets
Plug 'jiangmiao/auto-pairs'

" I like autoclosing brackets; naturally, I like not writing the 'end'
" of functions and blocks.
Plug 'tpope/vim-endwise'

" Why am I putting this here? Well, this makes it easy to find who coded
" that awful line with more the 140 chars. This is used by gutentags to
" hide the tags file too, somehow.
Plug 'tpope/vim-fugitive'

" Seeing quicky which lines were modified is usefull.
Plug 'mhinz/vim-signify'

" Emmet for web dev stuff
Plug 'mattn/emmet-vim'

" Linting and syntax checking
Plug 'vim-syntastic/syntastic'
Plug 'myint/syntastic-extras'

" Color scheme
Plug 'arcticicestudio/nord-vim'
Plug 'hzchirs/vim-material'
Plug 'ronny/birds-of-paradise.vim'
Plug 'cocopon/iceberg.vim'
Plug 'nelstrom/vim-mac-classic-theme'
Plug 'dracula/vim'
Plug 'jpo/vim-railscasts-theme'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'ayu-theme/ayu-vim'
Plug 'dread-uo/NERV-ous'
Plug 'whatyouhide/gotham'

" This is awesome for completion! Using this with :set spell uses the dict to
" autocomplete, and feels very good. Works pretty well; I'm fonder of this
" than of deoplete these days.
Plug 'lifepillar/vim-mucomplete'

" Omnicompletion providers
Plug 'davidhalter/jedi-vim'

" Typescript definitions and autocomplete
Plug 'Quramy/tsuquyomi' | Plug 'Shougo/vimproc.vim', { 'do' : 'make' }

" C++ completion using ctags
Plug 'vim-scripts/OmniCppComplete'

" This is nice; I can have the configuration for identations on a separate
" file, without the autocmd mess. And it's easy to have a different
" configuration for projects with weird standards o/
Plug 'editorconfig/editorconfig-vim'

" Fancy multiple cursors, like sublime or atom or whatever
Plug 'terryma/vim-multiple-cursors'

" Running the tests from the editor seems nice! Hope it works.
Plug 'janko-m/vim-test'

" Look who is getting a fancy statusline! I've been postponing this for a bit,
" but the startup lag I've had with airline left me cautious about statusline
" plugins. Hope it really is light.
Plug 'itchyny/lightline.vim'

" I like this for presentations, or some simple writing. I like seeing the
" text on the center of the screen some times :p
Plug 'junegunn/goyo.vim'

" Just like with Goyo, this is nice for zen writing/coding, and for
" explanations as well.
Plug 'junegunn/limelight.vim'

" I've used (U|De)nite for some time, but these days it feels better to use
" fzf.
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin' }
Plug 'junegunn/fzf.vim'

" TODO:
" airline, or lighline. I'm also considering using the default status line with
"     syntastic info.

" language server (coc.nvim or ale, maybe)

" Autoformatting
" https://github.com/Chiel92/vim-autoformat
"
" gitgutter for quickly seeing changes
" https://github.com/airblade/vim-gitgutter

" Initialize plugin system
call plug#end()

colo nord

" Standard non-standard window movement
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Hitting space to start a search is rather fast, and feels good too.
nnoremap <Space> /

" Removing the highlights after searchs
nnoremap <silent> <ESC> :<C-u>noh<CR>

" Plugin configuration

" I like having the file tree sometimes :p
nnoremap <silent><F2> :<C-u>NERDTreeToggle<CR>

" vim-align config
nmap ga <Plug>(EasyAlign)

" fzf bindings
nnoremap <silent><leader>f :<C-u>FZF<CR>
nnoremap <leader>g :<C-u>Rg 

" This is the default setting; I might change those to use my leader in the
" future.
let g:fzf_action = {
      \ 'ctrl-t': 'tab split',
      \ 'ctrl-x': 'split',
      \ 'ctrl-v': 'vsplit' }

" Floating window, baby! And preview on rg
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }

" Some minimal mucomplete settings. Works great, as far as I can tell.
set completeopt+=menuone
set completeopt+=noselect
set shortmess+=c
let g:mucomplete#enable_auto_at_startup = 1

let g:syntastic_ruby_checkers = ["rubocop"]
let g:syntastic_python_checkers = ["pep8", "pycodestyle", "pyflakes"]
let g:syntastic_c_checkers = ["check", "clang_tidy"]
let g:syntastic_lua_checkers = ["luacheck"]
let g:syntastic_typescript_checkers = ["tsuquyomi"]
let g:syntastic_yaml_checkers = ["pyyaml"]

let g:vim_json_syntax_conceal = 1

" Lightline config
" Some happy funny faces are all I that my statusline was lacking. Maybe later
" we'll have some utility function to dynamically change the colorscheme, and
" some fugitive integration to show the current branch on a git repository.
let g:lightline = {
  \ 'colorscheme': 'nord',
  \ 'mode_map': {
  \   'n':      '(つ°ヮ°)つ',
  \   'i':      'ヽ(`Д´)ﾉ',
  \   'v':      '〳 ^ ▽ ^〵',
  \   'V':      '〳 ^ ▽ ^〵',
  \   '\<C-v>': '〳 ^ ▽ ^〵',
  \   'c':      'ヽ(”`▽´)ﾉ'
  \   }
  \ }

" There are times when I don't want the numbers showing on the left. I use
" relative numbers, so I need to unset two options, and it's boring to do
" by hand. Now it isn't boring, and I even get a neat left margin o/
function! ToggleNumbers()
  if &number
    set nonumber
    set norelativenumber
    set foldcolumn=2
  else
    set number
    set relativenumber
    set foldcolumn&
  endif
endfunction

nnoremap <silent><F3> :<C-u>call ToggleNumbers()<CR>
